<?php
/**
 * Created by PhpStorm.
 * User: miske
 * Date: 25.1.19.
 * Time: 13.06
 */

namespace Gdev\Awin\Models;


class Amount
{
    public $amount;
    public $currency;

    /**
     * Amount constructor.
     * @param float  $amount
     * @param string $currency
     */

    public function __construct(float $amount, string $currency)
    {
        $this->amount = $amount;
        $this->currency = $currency;
    }
}