<?php
/**
 * Created by PhpStorm.
 * User: miske
 * Date: 23.1.19.
 * Time: 14.20
 */

namespace Gdev\Awin\Models;


class Region
{
    public $name;
    public $countryCode;

    public function __construct($name,$countryCode)
    {
        $this->name = $name;
        $this->countryCode = $countryCode;
    }
}