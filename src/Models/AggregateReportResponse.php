<?php
/**
 * Created by PhpStorm.
 * User: miske
 * Date: 28.1.19.
 * Time: 10.28
 */

namespace Gdev\Awin\Models;


class AggregateReportResponse
{
    public $advertiserId;
    public $advertiserName;
    public $publisherId;
    public $publisherName;
    public $region;
    public $currency;
    public $impressions;
    public $clicks;
    public $creativeId;
    public $creativeName;
    public $pendingNo;
    public $pendingValue;
    public $pendingComm;
    public $confirmedNo;
    public $confirmedValue;
    public $confirmedComm;
    public $bonusNo;
    public $bonusValue;
    public $bonusComm;
    public $totalNo;
    public $totalValue;
    public $totalComm;
    public $declinedNo;
    public $declinedValue;
    public $declinedComm;

    public function countClicks(){
        echo $this->clicks;
    }
}