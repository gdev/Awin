<?php
include __DIR__ . '/../vendor/autoload.php';

use Gdev\Awin\AwinObjectDecorator;
use Gdev\Awin\Models\AccountsResponse;


class AwinObjecDecoratorTest extends \PHPUnit\Framework\TestCase
{

    private $awin;

    public function __construct()
    {
        parent::__construct();
        $this->awin = new AwinObjectDecorator(new \Gdev\Awin\AwinAdapter(AWIN_API_KEY));
    }

    public function testGetAllAccounts(): void
    {
        $response = $this->awin->getAllAccounts();
        $this->assertInstanceOf(
            AccountsResponse::class,
            $response
        );

        // test if results are correct
        $this->assertNotNull($response->userId);
        $this->assertIsInt($response->userId);
        $this->assertNotEmpty($response->accounts);
    }

}