<?php
/**
 * Created by PhpStorm.
 * User: miske
 * Date: 24.1.19.
 * Time: 11.09
 */

namespace Gdev\Awin\Models;


class Domain
{
    public $domain;

    /**
     * Domain constructor.
     * @param string $domain
     */

    public function __construct(string $domain)
    {
        $this->domain = $domain;
    }
}