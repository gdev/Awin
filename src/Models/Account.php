<?php

namespace Gdev\Awin\Models;


class Account
{
    public $accountId;
    public $accountName;
    public $accountType;
    public $userRole;

    public function hello() {
        return 'Hello, I am ss' . $this->accountName . ' and I am a ' . $this->userRole;
    }

    /**
     * Account constructor.
     * @param integer $accountId
     * @param string $accountName
     * @param string $accountType
     * @param string $userRole
     */
    public function __construct($accountId, $accountName, $accountType, $userRole)
    {
        $this->accountId = $accountId;
        $this->accountName = $accountName;
        $this->accountType = $accountType;
        $this->userRole = $userRole;
    }
}