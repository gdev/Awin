<?php


namespace Gdev\Awin\Models;

/**
 * Class AccountsResponse
 *
 * @property integer   userId
 * @property Account[] accounts
 */
class AccountsResponse
{

    public $userId;
    public $accounts = [];

    public function count() {
        return count($this->accounts);
    }

}