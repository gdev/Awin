<?php

namespace Gdev\Awin;


class AwinFactory
{
    public static function getObjectDecorator($awinAdapter): AwinObjectDecorator
    {
        return new AwinObjectDecorator($awinAdapter);
    }
}