<?php
/**
 * Created by PhpStorm.
 * User: miske
 * Date: 24.1.19.
 * Time: 16.59
 */

namespace Gdev\Awin\Models;


class CommissionGroup
{
    public $groupId;
    public $groupCode;
    public $groupName;
    public $type;
    public $amount;
    public $percentage;
    public $currency;

    /**
     * CommissionGroup constructor.
     * @param int         $groupId
     * @param string      $groupCode
     * @param string      $groupName
     * @param string      $type
     * @param int|null    $amount
     * @param int|null    $percentage
     * @param string|null $currency
     */
    public function __construct(int $groupId, string $groupCode, string $groupName, string $type, ?int $amount, ?int $percentage, ?string $currency)
    {
        $this->groupId = $groupId;
        $this->groupCode = $groupCode;
        $this->groupName = $groupName;
        $this->type = $type;
        $this->amount = $amount;
        $this->percentage = $percentage;
        $this->currency = $currency;
    }
}