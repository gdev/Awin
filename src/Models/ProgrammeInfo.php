<?php
/**
 * Created by PhpStorm.
 * User: miske
 * Date: 24.1.19.
 * Time: 11.15
 */

namespace Gdev\Awin\Models;


class ProgrammeInfo extends Programme
{
    public $validDomains = [];

    /**
     * ProgrammeInfo constructor.
     * @param int         $id
     * @param string      $name
     * @param string      $displayUrl
     * @param string      $clickThroughUrl
     * @param string|null $logoUrl
     * @param Region      $primaryRegion
     * @param string      $currencyCode
     * @param Domain[]    $validDomains
     */

    public function __construct(int $id, string $name, string $displayUrl, string $clickThroughUrl, ?string $logoUrl, Region $primaryRegion, string $currencyCode, array $validDomains)
    {
        parent::__construct($id, $name, $displayUrl, $clickThroughUrl, $logoUrl, $primaryRegion, $currencyCode);
        $this->validDomains = $validDomains;
    }

}