<?php
/**
 * Created by PhpStorm.
 * User: miske
 * Date: 23.1.19.
 * Time: 14.09
 */

namespace Gdev\Awin\Models;


class Programme
{
    public $id;
    public $name;
    public $displayUrl;
    public $clickThroughUrl;
    public $logoUrl;
    public $primaryRegion;
    public $currencyCode;

    /**
     * Account constructor.
     * @param integer $id
     * @param string  $name
     * @param string  $displayUrl
     * @param string  $clickThroughUrl
     * @param string  $logoUrl
     * @param Region  $primaryRegion
     * @param string  $currencyCode
     */

    public function __construct(int $id, string $name, string $displayUrl, string $clickThroughUrl, ?string $logoUrl, Region $primaryRegion, string $currencyCode)
    {
        $this->id = $id;
        $this->name = $name;
        $this->displayUrl = $displayUrl;
        $this->clickThroughUrl = $clickThroughUrl;
        $this->logoUrl = $logoUrl;
        $this->primaryRegion = $primaryRegion;
        $this->currencyCode = $currencyCode;
    }

}