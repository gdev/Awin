<?php
/**
 * Created by PhpStorm.
 * User: miske
 * Date: 25.1.19.
 * Time: 12.53
 */

namespace Gdev\Awin\Models;


class TransactionsListResponse
{
    public $id;
    public $url;
    public $advertiserId;
    public $publisherId;
    public $commissionSharingPublisherId;
    public $siteName;
    public $commissionStatus;
    public $commissionAmount;
    public $saleAmount;
    public $ipHash;
    public $customerCountry;
    public $clickRefs;
    public $clickDate;
    public $transactionDate;
    public $validationDate;
    public $type;
    public $declineReason;
    public $voucherCodeUsed;
    public $voucherCode;
    public $lapseTime;
    public $amended;
    public $amendReason;
    public $oldSaleAmount;
    public $oldCommissionAmount;
    public $clickDevice;
    public $transactionDevice;
    public $publisherUrl;
    public $advertiserCountry;
    public $orderRef;
    public $customParameters= [];
    public $transactionParts = [];
    public $paidToPublisher;
    public $paymentId;
    public $transactionQueryId;
    public $originalSaleAmount;

   public function renderPublisherId() {
       echo $this->id;
   }
}