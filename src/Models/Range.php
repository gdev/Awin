<?php
/**
 * Created by PhpStorm.
 * User: miske
 * Date: 24.1.19.
 * Time: 11.09
 */

namespace Gdev\Awin\Models;


class Range
{
    public $min;
    public $max;
    public $type;

    /**
     * Range constructor.
     * @param int    $min
     * @param int    $max
     * @param string $type
     */
    public function __construct(int $min, int $max, string $type)
    {
        $this->min = $min;
        $this->max = $max;
        $this->type = $type;
    }
}