<?php

namespace Gdev\Awin;

use DateTime;
use Gdev\Awin\Models\Account;
use Gdev\Awin\Models\AccountsResponse;
use Gdev\Awin\Models\AggregateReportResponse;
use Gdev\Awin\Models\Amount;
use Gdev\Awin\Models\ClickRefs;
use Gdev\Awin\Models\ComissionGroupResponse;
use Gdev\Awin\Models\CommissionGroup;
use Gdev\Awin\Models\CustomParameter;
use Gdev\Awin\Models\DataFeedResponse;
use Gdev\Awin\Models\Domain;
use Gdev\Awin\Models\Kpi;
use Gdev\Awin\Models\Programme;
use Gdev\Awin\Models\ProgrammeInfo;
use Gdev\Awin\Models\ProgrammeInfoResponse;
use Gdev\Awin\Models\ProgrammesResponse;
use Gdev\Awin\Models\Range;
use Gdev\Awin\Models\Region;
use Gdev\Awin\Models\TransactionParts;
use Gdev\Awin\Models\TransactionsListResponse;


class AwinObjectDecorator
{

    private $adapter;

    /**
     * AwinObjectDecorator constructor.
     * @param AwinAdapter $adapter
     */
    public function __construct(AwinAdapter $adapter)
    {
        $this->adapter = $adapter;
    }

    /**
     * @return AccountsResponse
     */
    public function getAllAccounts(): AccountsResponse
    {
        $response = $this->adapter->getAllAccounts();
        $result = new AccountsResponse();
        $result->userId = $response->body->userId;

        foreach ($response->body->accounts as $account) {
            $result->accounts[] = new Account($account->accountId, $account->accountName, $account->accountType, $account->userRole);
        }
        return $result;
    }

    /**
     * @param int         $accountId
     * @param string|null $relationship
     * @param string|null $countryCode
     * @return ProgrammesResponse
     */
    public function getProgrammes(int $accountId, string $relationship = null, string $countryCode = null): ProgrammesResponse
    {
        $response = $this->adapter->getProgrammes($accountId, $relationship, $countryCode);
        $result = new ProgrammesResponse();

        foreach ($response->body as $programme) {
            $result->programmes[] = new Programme(
                $programme->id,
                $programme->name,
                $programme->displayUrl,
                $programme->clickThroughUrl,
                $programme->logoUrl,
                new Region($programme->primaryRegion->name, $programme->primaryRegion->countryCode),
                $programme->currencyCode);
        }
        return $result;
    }

    /**
     * @param int $pubId
     * @param int $advertiserId
     * @return ProgrammeInfoResponse
     */
    public function getProgrammeInfo(int $pubId, int $advertiserId): ProgrammeInfoResponse
    {

        $response = $this->adapter->getProgrammeDetails($pubId, $advertiserId);
        $result = new ProgrammeInfoResponse();

        // handling programme info
        if (property_exists($response->body, 'programmeInfo')) {

            $programmeInfo = $response->body->programmeInfo;
            $validDomains = [];
            foreach ($programmeInfo->validDomains as $domain) {
                $validDomains[] = new Domain($domain->domain);
            }

            $result->programmeInfo = new ProgrammeInfo(
                $programmeInfo->id
                , $programmeInfo->name
                , $programmeInfo->displayUrl
                , $programmeInfo->clickThroughUrl
                , $programmeInfo->logoUrl
                , new Region($programmeInfo->primaryRegion->name, $programmeInfo->primaryRegion->countryCode)
                , $programmeInfo->currencyCode
                , $validDomains

            );
        }

        // handling kpi
        if (property_exists($response->body, 'kpi')) {

            $kpi = $response->body->kpi;
            $result->kpi = new Kpi(
                $kpi->averagePaymentTime
                , $kpi->approvalPercentage
                , $kpi->epc
                , $kpi->conversionRate
                , $kpi->validationdays
                , $kpi->awinIndex
            );
        }
        // handling commission range
        if (property_exists($response->body, 'commissionRange')) {
            foreach ($response->body->commissionRange as $range) {
                $result->commissionRange[] = new Range($range->min, $range->max, $range->type);
            }
        }
        return $result;
    }

    /**
     * @param int $pubId
     * @param int $advertiserId
     * @return ComissionGroupResponse
     */
    public function getCommissionGroup(int $pubId, int $advertiserId): ComissionGroupResponse
    {
        $response = $this->adapter->getCommissionGroups($pubId, $advertiserId);
        return $this->mapCommissionGroup($response);
    }

    public function getSingleCommissionGroup(int $pubId, int $groupId): ComissionGroupResponse
    {
        $response = $this->adapter->getCommissionGroup($pubId, $groupId);
        return $this->mapCommissionGroup($response);
    }

    private function mapCommissionGroup($response): ComissionGroupResponse
    {
        $result = new ComissionGroupResponse();

        //handling CommissionGroup
        if (property_exists($response->body, 'advertiser')) {
            $result->advertiser = $response->body->advertiser;
        }
        if (property_exists($response->body, 'publisher')) {
            $result->publisher = $response->body->publisher;
        }
        if (property_exists($response->body, 'commissionGroups')) {
            foreach ($response->body->commissionGroups as $group) {
                $result->commissionGroups[] = new CommissionGroup(
                    $group->groupId,
                    $group->groupCode,
                    $group->groupName,
                    $group->type,
                    property_exists($group, 'amount') ? $group->amount : null,
                    property_exists($group, 'percentage') ? $group->percentage : null,
                    property_exists($group, 'currency') ? $group->currency : null
                );
            }
        }
        return $result;
    }

    /**
     * @param int      $accountId
     * @param DateTime $startDate
     * @param DateTime $endDate
     * @param string   $timeZone
     * @return TransactionsListResponse
     */
    public function getPublisherTransactions(int $accountId, DateTime $startDate, DateTime $endDate, string $timeZone): TransactionsListResponse
    {

        $response = $this->adapter->getPublisherTransactions($accountId, $startDate, $endDate, $timeZone);
        return $this->mapTransactionList($response);
    }

    public function getAdvertiserTransaction(int $accountId, DateTime $startDate, DateTime $endDate, string $timeZone): TransactionsListResponse
    {
        $response = $this->adapter->getAdvertiserTransactions($accountId, $startDate, $endDate, $timeZone);
        return $this->mapTransactionList($response);
    }

    private function mapTransactionList($response): TransactionsListResponse
    {
        $result = new TransactionsListResponse();
        $body = $response->body;

        $result->id = $body->id;
        $result->url = $body->url;
        $result->advertiserId = $body->advertiserId;
        $result->publisherId = $body->publisherId;
        $result->commissionSharingPublisherId = $body->commissionSharingPublisherId;
        $result->siteName = $body->siteName;
        $result->commissionStatus = $body->commissionStatus;
        $result->commissionAmount = new Amount($body->commissionAmount->amount, $body->commissionAmount->currency);
        $result->saleAmount = new Amount($body->saleAmount->amount, $body->saleAmount->currency);
        $result->ipHash = $body->ipHash;
        $result->customerCountry = $body->customerCountry;
        $result->clickRefs = new ClickRefs(
            $body->clickRefs->clickRef
            , property_exists($body->clickRefs, 'clickRef2') ? $body->clickRefs->clickRef2 : null
            , property_exists($body->clickRefs, 'clickRef3') ? $body->clickRefs->clickRef3 : null
            , property_exists($body->clickRefs, 'clickRef4') ? $body->clickRefs->clickRef4 : null
            , property_exists($body->clickRefs, 'clickRef5') ? $body->clickRefs->clickRef5 : null
            , property_exists($body->clickRefs, 'clickRef6') ? $body->clickRefs->clickRef6 : null

        );
        $result->clickDate = new DateTime($body->clickDate);
        $result->transactionDate = new DateTime($body->transactionDate);
        $result->validationDate = $body->validationDate;
        $result->type = $body->type;
        $result->declineReason = $body->declineReason;
        $result->voucherCodeUsed = $body->voucherCodeUsed;
        $result->voucherCode = $body->voucherCode;
        $result->lapseTime = $body->lapseTime;
        $result->amended = $body->amended;
        $result->amendReason = $body->amendReason;
        $result->oldSaleAmount = $body->oldSaleAmount;
        $result->oldCommissionAmount = $body->oldCommissionAmount;
        $result->clickDevice = $body->clickDevice;
        $result->transactionDevice = $body->transactionDevice;
        $result->publisherUrl = $body->publisherUrl;
        $result->advertiserCountry = $body->advertiserCountry;
        $result->orderRef = $body->orderRef;
        foreach ($result->customParameters as $parameter) {
            $result->customParameters[] = new CustomParameter($parameter->key, $parameter->value);
        }
        foreach ($result->transactionParts as $transactionPart) {
            $result->transactionParts[] = new TransactionParts(
                $transactionPart->commissionGroupId,
                $transactionPart->amount,
                $transactionPart->commissionAmount,
                $transactionPart->commissionGroupCode,
                $transactionPart->commissionGroupName
            );
        }
        $result->paidToPublisher = $body->paidToPublisher;
        $result->paymentId = $body->paymentId;
        $result->transactionQueryId = $body->transactionQueryId;
        $result->originalSaleAmount = $body->originalSaleAmount;

        return $result;
    }

    public function getPublisherTransactionById(int $accountId, string $transactionId, string $timeZone): TransactionsListResponse
    {
        $response = $this->adapter->getPublishersTransactionById($accountId, $transactionId, $timeZone);
        return $this->mapTransactionList($response);

    }

    public function getAdvertiserTransactionById(int $accountId, string $transactionId, string $timeZone): TransactionsListResponse
    {
        $response = $this->adapter->getAdvertisersTransactionById($accountId, $transactionId, $timeZone);
        return $this->mapTransactionList($response);

    }

    private function mapAggregatedReport($response): AggregateReportResponse
    {
        $result = new AggregateReportResponse();

        $result->advertiserId = $response->body->advertiserId;
        $result->advertiserName = $response->body->advertiserName;
        $result->publisherId = $response->body->publisherId;
        $result->publisherName = $response->body->publisherName;
        $result->region = $response->body->region;
        $result->currency = $response->body->currency;
        $result->impressions = $response->body->impressions;
        $result->clicks = $response->body->clicks;
        property_exists($response->body, 'creativeId') ? $result->creativeId = $response->body->creativeId : null;
        property_exists($response->body, 'creativeName') ? $result->creativeName = $response->body->creativeName : null;
        $result->pendingNo = $response->body->pendingNo;
        $result->pendingValue = $response->body->pendingValue;
        $result->pendingComm = $response->body->pendingComm;
        $result->confirmedNo = $response->body->confirmedNo;
        $result->confirmedValue = $response->body->confirmedValue;
        $result->confirmedComm = $response->body->confirmedComm;
        $result->bonusNo = $response->body->bonusNo;
        $result->bonusValue = $response->body->bonusValue;
        $result->bonusComm = $response->body->bonusComm;
        $result->totalNo = $response->body->totalNo;
        $result->totalValue = $response->body->totalValue;
        $result->totalComm = $response->body->totalComm;
        $result->declinedNo = $response->body->declinedNo;
        $result->declinedValue = $response->body->declinedValue;
        $result->declinedComm = $response->body->declinedComm;

        return $result;
    }

    /**
     * @param int      $accountId
     * @param DateTime $startDate
     * @param DateTime $endDate
     * @param string   $region
     * @param string   $timeZone
     * @return AggregateReportResponse
     */
    public function getAggregatedByCreative(int $accountId, DateTime $startDate, DateTime $endDate, string $region, string $timeZone): AggregateReportResponse
    {
        $response = $this->adapter->getAggReportsByCreative($accountId, $startDate, $endDate, $region, $timeZone);
        return $this->mapAggregatedReport($response);
    }

    public function getAggregatedByAdvertiser(int $accountId, DateTime $startDate, DateTime $endDate, string $region, string $timeZone): AggregateReportResponse
    {
        $response = $this->adapter->getAggReportsByAdvertiser($accountId, $startDate, $endDate, $region, $timeZone);
        return $this->mapAggregatedReport($response);
    }

    public function getDataFeed($apiUrl, $path, $affiliateCode): DataFeedResponse
    {
        $response = new DataFeedResponse();
        $response->products = $this->adapter->getDataFeed($apiUrl, $path, $affiliateCode);
        return $response;
    }


}
