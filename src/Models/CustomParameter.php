<?php
/**
 * Created by PhpStorm.
 * User: miske
 * Date: 25.1.19.
 * Time: 13.29
 */

namespace Gdev\Awin\Models;


class CustomParameter
{
    public $key;
    public $value;


    public function __construct(string $key,string $value)
    {
        $this->key = $key;
        $this->value = $value;
    }
}