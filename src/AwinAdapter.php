<?php

namespace Gdev\Awin;

use DateTime;
use mysql_xdevapi\Exception;
use phpDocumentor\Reflection\Types\Object_;
use Unirest\Response;
use Unirest\Request;
use ZipArchive;

class AwinAdapter
{

    public const API_URL = 'https://api.awin.com';
    private $accessToken;
    private $headers;

    /**
     * AwinAdapter constructor.
     * @param string $accessToken
     * @param array  $headers
     */
    public function __construct(string $accessToken, array $headers = ['Accept' => 'application/json'])
    {
        $this->accessToken = $accessToken;
        $this->setHeaders($headers);
    }

    //region setters

    public function setHeaders($headers): void
    {
        $this->headers = $headers;
    }
    //endregion setters

    //region getters
    /**
     * @return Response
     * @throws AwinException
     */
    public function getAllAccounts(): Response
    {
        return $this->getAccounts();
    }

    /**
     * @param string $type
     * @return Response
     * @throws AwinException
     */
    private function getAccounts(string $type = null): Response
    {
        $apiUrl = static::API_URL;
        $url = "{$apiUrl}/accounts/";
        $params = [
            'accessToken' => $this->accessToken,
        ];
        // check type
        if ($type !== null) {
            $params['type'] = $type;
        }
        return $this->getResponse($url, $this->headers, $params);
    }


    /**
     * @param int         $accountID
     * @param string|null $relationship
     * @param string|null $countryCode
     * @return Response
     * @throws AwinException
     */
    public function getProgrammes(int $accountID, string $relationship = null, string $countryCode = null): Response
    {
        $countryCode = strtoupper($countryCode);
        $apiUrl = static::API_URL;
        $url = "{$apiUrl}/publishers/{$accountID}/programmes";
        $params = [
            'accessToken' => $this->accessToken,
            'relationship' => $relationship,        //relationship type can be joined,pending suspended,rejected not joined
            'countryCode' => $countryCode
        ];

        return $this->getResponse($url, $this->headers, $params);
    }

    /**
     * @return Response
     * @throws AwinException
     */
    public function getPublisherAccounts(): Response
    {
        return $this->getAccounts('publisher');
    }

    /**
     * @return Response
     */

    public function getAdvertiserAccounts(): Response
    {
        return $this->getAccounts('advertiser');
    }

    //endregion getters

    /**
     * Publishers get a set of KPIs of the programmes they are working with,
     * furthermore they additionally get the range of commission (across commissiongroups) they gets in the programmes.
     * @param int $pubId
     * @param int $advertiserId
     * @return Response
     */

    public function getProgrammeDetails(int $pubId, int $advertiserId): Response
    {
        $apiUrl = static::API_URL;
        $url = "{$apiUrl}/publishers/{$pubId}/programmedetails";
        $params = [
            'accessToken' => $this->accessToken,
            'advertiserId' => $advertiserId
        ];


        $response = Request::get($url, $this->headers, $params);

        return $response;
    }

    /**Publishers can request all commission groups of a programme, together with the commission values they get.
     * Furthermore it is possible to just pull the commission value for a single commission group.
     * method to
     * @param int $pubId
     * @param int $advertiserId
     * @return Response
     */

    public function getCommissionGroups(int $pubId, int $advertiserId): Response
    {
        $apiUrl = static::API_URL;
        $url = "{$apiUrl}/publishers/{$pubId}/commissiongroups";
        $params = [
            'accessToken' => $this->accessToken,
            'advertiserId' => $advertiserId
        ];

        $response = Request::get($url, $this->headers, $params);

        return $response;
    }

    /**Publishers pull the commission value for a single commission group.
     * @param int $pubId
     * @param int $groupId
     * @return Response
     */
    public function getCommissionGroup(int $pubId, int $groupId): Response
    {
        $apiUrl = static::API_URL;
        $url = "{$apiUrl}/publishers/{$pubId}/commissiongroups/$groupId";
        $params = [
            'accessToken' => $this->accessToken,
        ];

        $response = Request::get($url, $this->headers, $params);

        return $response;
    }

    /**
     * * method to get list of transaction
     * @param string   $type
     * @param int      $accountId
     * @param DateTime $startDate
     * @param DateTime $endDate
     * @param string   $timeZone
     * @return Response
     */
    private function getTransactionsList(string $type, int $accountId, DateTime $startDate, DateTime $endDate, string $timeZone): Response
    {
        $apiUrl = static::API_URL;
        $url = "{$apiUrl}/{$type}/{$accountId}/transactions/";

        $params = [
            'accessToken' => $this->accessToken,
            'startDate' => $startDate->format("Y-m-d\TH:i:s"),
            'endDate' => $endDate->format("Y-m-d\TH:i:s"),
            'timeZone' => $timeZone
        ];

        $response = Request::get($url, $this->headers, $params);
        return $response;
    }

    /**
     * method to get publisher transactions list
     * @param int      $accountId
     * @param DateTime $startDate
     * @param DateTime $endDate
     * @param string   $timeZone
     * @return Response
     */
    public function getPublisherTransactions(int $accountId, DateTime $startDate, DateTime $endDate, string $timeZone): Response
    {
        return $this->getTransactionsList('publishers', $accountId, $startDate, $endDate, $timeZone);
    }

    /**
     *  method to get advertiser transactions list
     * @param int      $accountId
     * @param DateTime $startDate
     * @param DateTime $endDate
     * @param string   $timeZone
     * @return Response
     */
    public function getAdvertiserTransactions(int $accountId, DateTime $startDate, DateTime $endDate, string $timeZone): Response
    {
        return $this->getTransactionsList('advertisers', $accountId, $startDate, $endDate, $timeZone);
    }

    /**
     * method to get transaction by id
     * @param string $type
     * @param int    $accountId
     * @param string $transactionId
     * @param string $timeZone
     * @return Response
     */

    private function getTransactionById(string $type, int $accountId, string $transactionId, string $timeZone): Response
    {
        $apiUrl = static::API_URL;
        $url = "{$apiUrl}/{$type}/{$accountId}/transactions/";

        $params = [
            'accessToken' => $this->accessToken,
            'ids' => $transactionId,
            'timeZone' => $timeZone
        ];

        $response = Request::get($url, $this->headers, $params);
        return $response;
    }

    /**
     * Method to get publishers transactions by id
     * @param int    $accountId
     * @param string $transactionId
     * @param string $timeZone
     * @return Response
     */

    public function getPublishersTransactionById(int $accountId, string $transactionId, string $timeZone): Response
    {
        return $this->getTransactionById('publishers', $accountId, $transactionId, $timeZone);

    }

    /**
     * * Method to get advertisers transactions by id
     * @param int    $accountId
     * @param string $transactionId
     * @param string $timeZone
     * @return Response
     */

    public function getAdvertisersTransactionById(int $accountId, string $transactionId, string $timeZone): Response
    {
        return $this->getTransactionById('advertisers', $accountId, $transactionId, $timeZone);

    }

    /**
     * Method for performance report aggregates transactions, clicks and impressions for all advertisers a publisher
     * works with.
     * @param int      $accountId
     * @param DateTime $startDate
     * @param DateTime $endDate
     * @param string   $region
     * @param string   $timeZone
     * @return Response
     */
    public function getAggReportsByAdvertiser(int $accountId, DateTime $startDate, DateTime $endDate, string $region, string $timeZone): Response
    {

        $apiUrl = static::API_URL;
        $url = "{$apiUrl}/publishers/{$accountId}/reports/advertiser";

        $params = [

            'accessToken' => $this->accessToken,
            'accountId' => $accountId,
            'timeZone' => $timeZone,
            'startDate' => $startDate->format("Y-m-d"),
            'endDate' => $endDate->format("Y-m-d"),
            'region' => $region
        ];

        $response = Request::get($url, $this->headers, $params);
        return $response;
    }

    /**
     * Method for creative performance report aggregates transactions, clicks and impressions for the creatives and
     * publisher uses.
     * @param int      $accountId
     * @param DateTime $startDate
     * @param DateTime $endDate
     * @param string   $region
     * @param string   $timeZone
     * @return Response
     */
    public function getAggReportsByCreative(int $accountId, DateTime $startDate, DateTime $endDate, string $region, string $timeZone): Response
    {

        $apiUrl = static::API_URL;
        $url = "{$apiUrl}/publishers/{$accountId}/reports/creative";

        $params = [

            'accessToken' => $this->accessToken,
            'accountId' => $accountId,
            'timeZone' => $timeZone,
            'startDate' => $startDate->format("Y-m-d"),
            'endDate' => $endDate->format("Y-m-d"),
            'region' => $region
        ];

        $response = Request::get($url, $this->headers, $params);
        return $response;
    }

    private function getResponse($url, $headers, $params): Response
    {
        $response = Request::get($url, $headers, $params);
        if (property_exists($response->body, 'error')) {
            throw new AwinException($response->body->error . ' - ' . $response->body->description);
        }
        return $response;
    }

    /**
     * @param string $apiUrl
     * @param string $path
     * @param int    $affiliateCode
     * @return array
     */
    public function getDataFeed(string $apiUrl, string $path, int $affiliateCode): array
    {
        file_put_contents($path . 'Tmpfile.zip', fopen($apiUrl, 'rb'), $affiliateCode);
        $zip = new ZipArchive;
        $res = $zip->open($path . 'Tmpfile.zip');
        if ($res === TRUE) {
            $zip->extractTo($path);
            $zip->close();
        }
        $products = [];
        ini_set('auto_detect_line_endings', TRUE);
        $file = fopen("$path/datafeed_{$affiliateCode}.csv", 'rb');

        // get headers
        $headers = fgetcsv($file);

        // loop through projects
        $i = 0;
        while (!feof($file)) {
            $line = fgetcsv($file);
            $obj = new \stdClass();
            if ($line) {
                foreach ($line as $index => $data) {
                    $obj->{$headers[$index]} = $data;
                }
                $i++;
            }
            $products[] = $obj;
        }
        fclose($file);
        return $products;
    }

}
