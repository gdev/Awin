<?php
/**
 * Created by PhpStorm.
 * User: miske
 * Date: 24.1.19.
 * Time: 11.09
 */

namespace Gdev\Awin\Models;


class Kpi
{
    public $averagePaymentTime;
    public $approvalPercentage;
    public $epc;
    public $conversionRate;
    public $validationDays;
    public $awinIndex;

    public function __construct(string $averagePaymentTime,int $approvalPercentage,int $epc,int $conversionRate,int $validationDays,int $awinIndex)
    {
        $this->averagePaymentTime = $averagePaymentTime;
        $this->approvalPercentage = $approvalPercentage;
        $this->epc = $epc;
        $this->conversionRate = $conversionRate;
        $this->validationDays = $validationDays;
        $this->awinIndex = $awinIndex;
    }
}