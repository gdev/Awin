<?php
/**
 * Created by PhpStorm.
 * User: miske
 * Date: 25.1.19.
 * Time: 13.31
 */

namespace Gdev\Awin\Models;


class TransactionParts
{
    public $commissionGroupId;
    public $amount;
    public $commissionAmount;
    public $commissionGroupCode;
    public $commissionGroupName;

    public function __construct(int $commissionGroupId, int $amount, int $commissionAmount, string $commissionGroupCode, string $commissionGroupName)
    {
        $this->commissionGroupId = $commissionGroupId;
        $this->amount = $amount;
        $this->commissionAmount = $commissionAmount;
        $this->commissionGroupCode = $commissionGroupCode;
        $this->commissionGroupName = $commissionGroupName;
    }
}

