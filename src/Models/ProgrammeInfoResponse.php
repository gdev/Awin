<?php
/**
 * Created by PhpStorm.
 * User: miske
 * Date: 24.1.19.
 * Time: 11.07
 */

namespace Gdev\Awin\Models;


class ProgrammeInfoResponse
{
    public $programmeInfo;
    public $kpi;
    public $commissionRange = [];

    public function renderResponse(): void
    {
        var_dump($this->programmeInfo);
        var_dump($this->kpi);
        var_dump($this->commissionRange);
    }
}