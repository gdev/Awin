<?php
/**
 * Created by PhpStorm.
 * User: miske
 * Date: 25.1.19.
 * Time: 13.25
 */

namespace Gdev\Awin\Models;


class ClickRefs
{
    public $clickRef;
    public $clickRef2;
    public $clickRef3;
    public $clickRef4;
    public $clickRef5;
    public $clickRef6;

    /**
     * ClickRefs constructor.
     * @param string      $clickRef
     * @param string|null $clickRef2
     * @param string|null $clickRef3
     * @param string|null $clickRef4
     * @param string|null $clickRef5
     * @param string|null $clickRef6
     */
    public function __construct(string $clickRef, ?string $clickRef2, ?string $clickRef3, ?string $clickRef4, ?string $clickRef5, ?string $clickRef6)
    {
        $this->clickRef =  $clickRef;
        $this->clickRef2 = $clickRef2;
        $this->clickRef3 = $clickRef3;
        $this->clickRef4 = $clickRef4;
        $this->clickRef5 = $clickRef5;
        $this->clickRef6 = $clickRef6;
    }
}